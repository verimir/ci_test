import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;
@Epic("AT")
@Feature("demo test class")

public class DemoTest {

    @Test(description = "demo test method")
    public void demoTest(){
        System.out.println("demo test was executed");
    }
}
